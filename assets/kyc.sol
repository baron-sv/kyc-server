pragma solidity ^ 0.4.21;

contract UserInfo {

  struct UserStruct {
    string accountName;
    string accountEmail;
    address accountId;
    uint amount;
    string password;
    string userStatus;
    string userType;
    uint index;
  }
  
  mapping(address => UserStruct) private userStructs;
  address[] private userIndex;

  function isUser(address userAddress) public constant returns(bool isIndeed) {
    
    if(userIndex.length == 0) return false;
    return (userIndex[userStructs[userAddress].index] == userAddress);
  }

  function insertUser(address userAddress, string accountName, string accountEmail, string password, uint amount, string userStatus, string userType) public returns(uint index)  {
    
    if(isUser(userAddress)) throw; 
    userStructs[userAddress].accountName = accountName;
    userStructs[userAddress].accountEmail = accountEmail;
	  userStructs[userAddress].accountId = userAddress;
    userStructs[userAddress].password = password;
	  userStructs[userAddress].amount = amount;
    userStructs[userAddress].userStatus = userStatus;
    userStructs[userAddress].userType = userType;
    userStructs[userAddress].index = userIndex.push(userAddress)-1;
    return userIndex.length-1;
  }
  
  function getUser(address userAddress) public constant returns(address accountId, string accountName, string accountEmail, string password, uint amount, string userType, uint index) {
    if(!isUser(userAddress)) throw; 
    return( userStructs[userAddress].accountId, userStructs[userAddress].accountName, userStructs[userAddress].accountEmail, userStructs[userAddress].password, userStructs[userAddress].amount, userStructs[userAddress].userType,  userStructs[userAddress].index);
  }
  
  function updateaccountName(address userAddress, string accountName)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].accountName = accountName;
    return true;
  }
  
  function updateamount(address userAddress, uint amount)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].amount = amount;
    return true;
  }
  
  function updateStaus(address userAddress, string userStatus)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].userStatus = userStatus;
    return true;
  }

  function getUserCount()  public constant returns(uint count) {
    return userIndex.length;
  }

  function getUserAtIndex(uint index) public constant returns(address userAddress) {
    return userIndex[index];
  }
  
  function getallUser() public constant returns(address[]) {
    return( userIndex);
  }

}

contract UserPersonalInfo {

  struct PersonalInfoStructs {
    address userAddress;
    string name;
    string gender;
    string userHomeAddress;
    uint phone;
    string kycInformation;
    string dataAccessInfo;
    string identifier;
    string status;
    uint index;
  }
  
  struct KycInfo {
    string identifier;
    string kycDetails;
    uint index;
  }
  
  mapping(address => PersonalInfoStructs) private personalInfoStructs;
  mapping(address => KycInfo[]) kycInfo;
  
  address[] private userIndex;
  address[] private kycIndex;
  
  function isValidUser(address userAddress) public constant returns(bool isIndeed) {
    
    if(userIndex.length == 0) return false;
    return (userIndex[personalInfoStructs[userAddress].index] == userAddress);
  }

  function insertPersonalInfo(address userAddress, string name, string gender, string userHomeAddress, uint phone) public returns(uint index)  {
    
    if(isValidUser(userAddress)) throw; 
      personalInfoStructs[userAddress].name = name;
      personalInfoStructs[userAddress].gender   = gender;
      personalInfoStructs[userAddress].userHomeAddress   = userHomeAddress;
      personalInfoStructs[userAddress].phone   = phone;
      personalInfoStructs[userAddress].identifier   = "";
      personalInfoStructs[userAddress].kycInformation   = "";
      personalInfoStructs[userAddress].status = "pending";
      personalInfoStructs[userAddress].dataAccessInfo = "";
      personalInfoStructs[userAddress].index     = userIndex.push(userAddress)-1;
      
      return userIndex.length-1;
    }
  
  function updatePersonalInfo(address userAddress, string name, string gender, string userHomeAddress, uint phone)  public returns(bool success)  {
    
    if(!isValidUser(userAddress)) throw; 
      personalInfoStructs[userAddress].name = name;
      personalInfoStructs[userAddress].gender   = gender;
      personalInfoStructs[userAddress].userHomeAddress   = userHomeAddress;
      personalInfoStructs[userAddress].phone   = phone;
      personalInfoStructs[userAddress].index     = userIndex.push(userAddress)-1;
      return true;
    }
  
  function getUsersPersonalInfo(address userAddress) public constant returns( string name, string gender, string userHomeAddress, uint phone, string dataAccessInfo, uint index) {
    
    if(!isValidUser(userAddress)) throw; 
      return( personalInfoStructs[userAddress].name, personalInfoStructs[userAddress].gender, personalInfoStructs[userAddress].userHomeAddress, personalInfoStructs[userAddress].phone, personalInfoStructs[userAddress].dataAccessInfo, personalInfoStructs[userAddress].index); 
    }
  

  function getPersonalInfoCount()  public constant returns(uint count) {
      return userIndex.length;
  }

  function getUserAtIndex(uint index) public constant returns(address userAddress) {
      return userIndex[index];
  }
  
  function getallUserPersonalInfo() public constant returns(address[]) {
    
      return( userIndex);
  }
  
  function updateKYC(address userAddress, string identifier, string kycInformation, string dataAccessInfo) public returns(bool success) {
       personalInfoStructs[userAddress].identifier = identifier;
       personalInfoStructs[userAddress].kycInformation = kycInformation;
       personalInfoStructs[userAddress].dataAccessInfo = dataAccessInfo;
       return true;
  }

  function getUsersKycInfo(address userAddress) public constant returns( string name, string identifier, string kycInformation, string dataAccessInfo, string status) {
      if(!isValidUser(userAddress)) throw; 
      return( personalInfoStructs[userAddress].name, personalInfoStructs[userAddress].identifier, personalInfoStructs[userAddress].kycInformation, personalInfoStructs[userAddress].dataAccessInfo, personalInfoStructs[userAddress].status);
  }

  function updateStaus(address userAddress, string userStatus)  public returns(bool success) {
    personalInfoStructs[userAddress].status = userStatus;
    return true;
  }

  function updateAccessInfo(address userAddress, string dataAccessInfo)  public returns(bool success) {
    personalInfoStructs[userAddress].dataAccessInfo = dataAccessInfo;
    return true;
  }
}