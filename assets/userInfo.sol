pragma solidity ^ 0.4.21;

contract UserInfo {

  struct UserStruct {
    string accountName;
    string accountEmail;
    address accountId;
    uint amount;
    string password;
    string userStatus;
    string userType;
    uint index;
  }
  
  mapping(address => UserStruct) private userStructs;
  address[] private userIndex;

  function isUser(address userAddress) public constant returns(bool isIndeed) {
    
    if(userIndex.length == 0) return false;
    return (userIndex[userStructs[userAddress].index] == userAddress);
  }

  function insertUser(address userAddress, string accountName, string accountEmail, string password, uint amount, string userStatus, string userType) public returns(uint index)  {
    
    if(isUser(userAddress)) throw; 
    userStructs[userAddress].accountName = accountName;
    userStructs[userAddress].accountEmail = accountEmail;
	  userStructs[userAddress].accountId = userAddress;
    userStructs[userAddress].password = password;
	  userStructs[userAddress].amount = amount;
    userStructs[userAddress].userStatus = userStatus;
    userStructs[userAddress].userType = userType;
    userStructs[userAddress].index = userIndex.push(userAddress)-1;
    return userIndex.length-1;
  }
  
  function getUser(address userAddress) public constant returns(address accountId, string accountName, string accountEmail, string password, uint amount, string userType, uint index) {
    if(!isUser(userAddress)) throw; 
    return( userStructs[userAddress].accountId, userStructs[userAddress].accountName, userStructs[userAddress].accountEmail, userStructs[userAddress].password, userStructs[userAddress].amount, userStructs[userAddress].userType,  userStructs[userAddress].index);
  }
  
  function updateaccountName(address userAddress, string accountName)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].accountName = accountName;
    return true;
  }
  
  function updateamount(address userAddress, uint amount)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].amount = amount;
    return true;
  }
  
  function updateStaus(address userAddress, string userStatus)  public returns(bool success) {
    if(!isUser(userAddress)) throw; 
    userStructs[userAddress].userStatus = userStatus;
    return true;
  }

  function getUserCount()  public constant returns(uint count) {
    return userIndex.length;
  }

  function getUserAtIndex(uint index) public constant returns(address userAddress) {
    return userIndex[index];
  }
  
  function getallUser() public constant returns(address[]) {
    return( userIndex);
  }

}

contract UserPersonalInfo {

  struct PersonalInfoStructs {
    address userAddress;
    string name;
    string dob;
    string userHomeAddress;
    uint phone;
    string kycInformation;
    uint index;
  }
  
  struct KycInfo {
    string kycDetails;
    uint index;
  }
  
  mapping(address => PersonalInfoStructs) private personalInfoStructs;
  mapping(address => KycInfo) kycInfo;
  
  address[] private userIndex;
  address[] private kycIndex;
  
  function isValidUser(address userAddress) public constant returns(bool isIndeed) {
    
    if(userIndex.length == 0) return false;
    return (userIndex[personalInfoStructs[userAddress].index] == userAddress);
  }

  function insertPersonalInfo(address userAddress, string name, string dob, string userHomeAddress, uint phone) public returns(uint index)  {
    
    if(isValidUser(userAddress)) throw; 
      personalInfoStructs[userAddress].name = name;
      personalInfoStructs[userAddress].dob   = dob;
      personalInfoStructs[userAddress].userHomeAddress   = userHomeAddress;
      personalInfoStructs[userAddress].phone   = phone;
      personalInfoStructs[userAddress].kycInformation   = "";
      personalInfoStructs[userAddress].index     = userIndex.push(userAddress)-1;
      return userIndex.length-1;
    }
  
  function updatePersonalInfo(address userAddress, string name, string dob, string userHomeAddress, uint phone)  public returns(bool success)  {
    
    if(!isValidUser(userAddress)) throw; 
      personalInfoStructs[userAddress].name = name;
      personalInfoStructs[userAddress].dob   = dob;
      personalInfoStructs[userAddress].userHomeAddress   = userHomeAddress;
      personalInfoStructs[userAddress].phone   = phone;
      personalInfoStructs[userAddress].index     = userIndex.push(userAddress)-1;
      return true;
    }
  
  function getUsersPersonalInfo(address userAddress) public constant returns( string name, string dob, string userHomeAddress, uint phone, uint index) {
    
    if(!isValidUser(userAddress)) throw; 
      return( personalInfoStructs[userAddress].name, personalInfoStructs[userAddress].dob, personalInfoStructs[userAddress].userHomeAddress, personalInfoStructs[userAddress].phone, personalInfoStructs[userAddress].index); 
    }
  

  function getPersonalInfoCount()  public constant returns(uint count) {
      return userIndex.length;
  }

  function getUserAtIndex(uint index) public constant returns(address userAddress) {
      return userIndex[index];
  }
  
  function getallUserPersonalInfo() public constant returns(address[]) {
    
      return( userIndex);
  }
  
  function updateKYC(address userAddress, string userKycDetails) public returns(bool success) {
       //kycInfo[userAddress].kycDetails.push(userKycDetails);
       kycInfo[userAddress].kycDetails = userKycDetails;
       kycInfo[userAddress].index     = kycIndex.push(userAddress)-1;
       return true;
  }

  function getUsersKycInfo(address userAddress) public constant returns( string kycDetails) {
      //if(!isValidUser(userAddress)) throw; 
      return( kycInfo[userAddress].kycDetails);
  }
  
  function getKycInfoCount()  public constant returns(uint count) {
      return kycIndex.length;
  }
  
  function getallUserKycInfo() public constant returns(address[]) {
    
      return( kycIndex);
  }

  function getPersonalKycInfo(address userAddress) public constant returns( string name, string kycDetails) {
      //if(!isValidUser(userAddress)) throw; 
      return( personalInfoStructs[userAddress].name, kycInfo[userAddress].kycDetails);
  }
}