var bodyParser = require("body-parser");
var express = require("express");
var errorHandler = require("errorhandler");
var cors = require('cors');
var index_1 = require("./routes/index");
var fileUpload = require('express-fileupload');
var swagger = require('swagger-express');
//var busboy = require('connect-busboy');


var Server = (function () {
    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    function Server() {
        //create expressjs application
        this.app = express();
        //configure application
        this.config();
        //add routes
        this.routes();
        //add api
        this.api();
    }
    Server.bootstrap = function () {
        return new Server();
    };
    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    Server.prototype.api = function () {
        //empty for now
    };
    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    Server.prototype.config = function () {
        //mount json form parser
        this.app.use(bodyParser.json({limit: '300mb'}));
        //this.app.use(busboy());
        //this.app.use(busboy({ immediate: true }));


        this.app.use(fileUpload());

        this.app.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', '*');
        
            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        
            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            res.setHeader('Access-Control-Allow-Credentials', true);
        
            // Pass to next layer of middleware
            next();
        });
        //mount query string parser
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(cors());
        this.app.use(express.static('public/dist/kyc-ui'));
        this.app.use(express.static('public/dist/network'));
        this.app.get('/login', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        this.app.get('/accountmanage', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        this.app.get('/deploy/deploycontract', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        this.app.get('/analytics', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        this.app.get('/scheduling', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        this.app.get('/blockexplorer', function (req, res) {
            var uid = req.params.uid, path = req.params[0] ? req.params[0] : 'index.html';
            res.sendFile(path, { root: 'public/dist/kyc-ui' });
        });
        // catch 404 and forward to error handler
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        //error handling
        this.app.use(errorHandler());

        this.app.use(swagger.init(this.app, {
            apiVersion: '1.0',
            swaggerVersion: '2.0',
            swaggerURL: '/docs',
            swaggerJSON: './assets/doc.json',
            swaggerUI: './public/swagger/',
            basePath: 'http://localhost:5000',
            apis: ['./src/server/routes/index.js'],
            middleware: function (req, res) {
            }
        })); 
    };
    Server.prototype.routes = function () {
        var router;
        router = express.Router();
        //IndexRoute
        index_1.IndexRoute.create(router);
        //use router middleware
        this.app.use(router);
    };
    return Server;
})();
exports.Server = Server;
