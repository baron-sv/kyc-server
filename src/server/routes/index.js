var solc = require("solc");
var fs = require("fs");
var request = require("request");
var Web3 = require("web3");
var ipfsAPI = require('ipfs-api');
var fileUpload = require('express-fileupload');
var Docker = require('dockerode');
var docker = new Docker({socketPath: '/var/run/docker.sock'});
var getPort = require('get-port');
var blockchainurl = process.env.BLOCKCHAIN_SERVICE_URL || 'http://localhost:8545';
var web3 = new Web3(new Web3.providers.HttpProvider(blockchainurl));
var solFile = fs.readFileSync('./assets/kyc.sol', "utf8");
var compiledObject = solc.compile(solFile, 1);
var keyMapUserInfo = {};
var keyMapUserKycInfo = {};
var ipfsAPIHost = process.env.IPFS_API_HOST || '13.127.233.189';
var ipfsAPIPort = process.env.IPFS_API_PORT || '5000';
var ipfs = ipfsAPI({host: ipfsAPIHost, port: ipfsAPIPort});
var ipfsService = process.env.IPFS_SERVICE_URL || 'http://13.127.233.189/ipfs';
var ipfsServiceURL = {url:  ipfsService};
var keyUserTransaction=[];
var map = [];
var keyBankTransaction=[];
var bankMap = [];
/**
 * / route
 *
 * @class User
 */
var IndexRoute = (function () {
    function IndexRoute() {
    }
    /**
     * Create the routes.
     *
     * @class Ind-exRoute
     * @method create
     * @static
     */
    IndexRoute.create = function (router) {
        
        router.get("/", function (req, res, next) {
            res.redirect('/docs');
        });

        router.get("/doc.json", function (req, res, next) {
            var docjson = fs.readFileSync('./assets/doc.json', "utf8");
            res.header('Access-Control-Allow-Origin', '*');
            res.download('./assets/doc.json');
        });
        
        router.get("/bcurl", function (req, res, next) {
            var blockchainurl = process.env.BLOCKCHAIN_SERVICE_URL;
            var privateIp = process.env.HEROKU_PRIVATE_IP;
            res.send({ "blockchainurl": blockchainurl, "privateIp": privateIp});
        });
		
        router.post("/create/account", async function (req, res, next) {
            try {
                var requestData = req.body;
                console.log("... requestData", requestData);
                await deployUserContract();
                var accountId = await createBCUser(requestData.username, requestData.password);
                var newUser = [accountId, requestData.username, requestData.email, requestData.password, 0, 'active', 'user'];
                console.log("...newUser", newUser);
                await createUser(newUser);
                await getUserDetails(); 
                res.status(200);
                res.json('User created successfully');
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.post("/create/bank/account", async function (req, res, next) {
            try {
                var requestData = req.body;
                await deployUserContract();
                console.log("...userInfo", requestData);
                await createUser(requestData);
                await getUserDetails();
                res.status(200);
                res.json('User created successfully');
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.post("/auth/user", async function (req, res, next) {
            try {
                    requestData = req.body;
                    accountDetails = await getUserDetails();
                    console.log("localStorage requestData", accountDetails);
                    var authUser = '';
                    var userStatus = '';
                    await accountDetails.filter(accountDetail => {
                        console.log("localStorage accountDetail", accountDetail);
                        if (requestData.username == accountDetail.accountName && requestData.password == accountDetail.password) {
                            console.log("authUser accountDetail", accountDetail);
                            authUser = accountDetail;
                            userStatus = accountDetail.status ? accountDetail.status : 'active';
                            console.log("authUser authUser", authUser);
                        }
                    });
                    if( authUser != '' && userStatus == 'active') {
                        res.status(200);
                        res.send(authUser);
                        console.log("localStorage authUser", requestData);
                    } else if (authUser != '' && userStatus == 'inactive') {
                        res.status(401);
                        console.log("localStorage 401", requestData);
                        res.send({ errorMessage: 'User account has inactive!!' });
                    } else {
                        res.status(401);
                        console.log("localStorage 401", requestData);
                        res.send({ errorMessage: 'Username or Password is not invalid' });
                    }
            }  catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });
        
        router.get("/accounts", async function (req, res, next) {
            var userInfo = await getUserDetails('bank');
            res.status(200);
            res.json(userInfo);
        });

        router.post("/update/account", async function (req, res, next) {
            try {
                var requestData = req.body;
                console.log("request Data", requestData);
                var updateUsersInfo = await updateUserInfo(requestData.method, requestData.accountId, requestData.data);
                if(updateUserInfo) {
                    res.status(200);
                    res.json("Success");
                }
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });
        router.post("/update/kyc/:accountId", async function (req, res, next) {
           
            let sampleFile = req.files.file;
            var requestData = req.body;
            var requestParams = req.params;
            var dataAccessInfo = JSON.stringify([{"idtype": true,	"idInfo": false,"name": "true",	"filehash": false}]);
            try {
                await sampleFile.mv('./assets/ipfsupload/'+req.files.file.name, async function(err) {
                    if (err)
                    return res.status(500).send(err);
                    var filePath='./assets/ipfsupload/'+req.files.file.name;
                    let files = [{
                                content:fs.readFileSync(filePath)
                                }]
                    await ipfs.files.add(files, async function (err, files) {
                        var userKycDetail = await getUserKycInfo("getUsersKycInfo", requestParams.accountId);
                        if(err) {
                            console.log("uploadFilesToIpfs", err);
                        } else {
                            fileHash = await files[0].hash;
                            console.log("fileHash fileHash ", fileHash);
                            await fs.unlink(filePath, (err) => {
                                if (err) 
                                    console.log('Unable to delete file', fileHash);
                                else
                                    console.log('File deleted from filesystem');
                            });
                            var ipfsFileHash = ipfsServiceURL.url+'/'+fileHash;
                            var kycData = [];
                            console.log("userKycDetail ", userKycDetail)
                            if(userKycDetail.kycInformation != '') {
                                var userKycDetails = JSON.parse(userKycDetail.kycInformation);
                                for(var i=0; i< userKycDetails.length; i++) {
                                    kycData.push(userKycDetails[i])
                                }
                            }
                            var identifier = requestData.uniqueId ? requestData.uniqueId : 'welcome';
                            kycData.push({idtype: requestData.idtype, idInfo: requestData.idInfo, name: requestData.name, filehash: ipfsFileHash});
                            var kycDatas = [{idtype: requestData.idtype, idInfo: requestData.idInfo, name: requestData.name, filehash: ipfsFileHash}]
                            var kycDetails = [identifier, JSON.stringify(kycData), dataAccessInfo]
                            await updateUserKycInfo("updateKYC", requestParams.accountId, kycDetails);
                        };
                    });
                });
                res.status(200);
                res.json("Success");
            } catch (error) {
                return res.status(500).send("KYC Update Error", error);
            }
        });

        router.post("/update/personal/:accountId/", async function (req, res, next) {
            console.log("COMING.... ")
            try {
                var requestData = req.body;
                var requestParams = req.params;
                await deployUserKycContract();
                var name = requestData.firstname+' '+requestData.lastname
                var personalInfo = [name, requestData.gender, requestData.address, requestData.mobile];
                await updateUserKycInfo("insertPersonalInfo", requestParams.accountId, personalInfo);
                res.status(200);
                res.json("Success");
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.get("/personal/:accountId/", async function (req, res, next) {
            try {
                await deployUserKycContract();
                var requestData = req.body;
                var requestParams = req.params;
                var personalDetail = await getUserKycInfo("getUsersPersonalInfo", requestParams.accountId);
                res.status(200);
                res.json(JSON.stringify(personalDetail));
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.get("/kyc/:accountId/", async function (req, res, next) {
            console.log("COMING AccountId.... ")
            try {
                var requestData = req.body;
                var requestParams = req.params;
                await deployUserKycContract();
                var kycDetail = await getUserKycInfo("getUsersKycInfo", requestParams.accountId);
                res.status(200);
                res.json(JSON.stringify(kycDetail));
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.get("/kyc", async function (req, res, next) {
            console.log("COMING AccountId.... ")
            try {
                var requestData = req.body;
                var requestParams = req.params;
                var requestQuery= req.query;
                var kycInformation = await getAllKycInfo(requestQuery.identifier);
                res.status(200);
                res.send(JSON.stringify(kycInformation));
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.get("/bank", async function (req, res, next) {
            var userInfo = await getBankUserDetails();
            res.status(200);
            res.json(userInfo);
        });

        router.get("/transactions/:accountId", async function (req, res, next) {
            var requestParams = req.params;
            var requestQuery= req.query;
            var bankAddress = requestQuery.bankAddress ? requestQuery.bankAddress : '';
            var transactionInfo;
            if(bankAddress == '') {
                transactionInfo = await getUserTransaction(requestParams.accountId);
            } else {
                transactionInfo = await getBankTransaction(requestParams.accountId);
            }
            res.status(200);
            res.json(transactionInfo);
        });

        router.get("/kyc/search", async function (req, res, next) {
            //var userInfo = await getBankUserDetails();
            var requestQuery= req.query;
            var kycInfo = {name: "Welcome", address: "Nungambakkam" , status: "verified", identifier : "identifier"}
            res.status(200);
            res.json(kycInfo);
        });

        router.get("/transactions/:accountId/:startBlock/:endBlock", async function (req, res, next) {
            console.log("Transactions AccountId.... ")
            try {
                var requestData = req.body;
                var requestParams = req.params;
                var transactionDetails = await getTransactionsByAccount(requestParams.accountId, requestParams.startBlock, requestParams.endBlock);
                res.status(200);
                res.json(transactionDetails);
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        router.get("/update/status", async function (req, res, next) {
            try {
                var requestData = req.body;
                var requestParams = req.params;
                var requestQuery= req.query;
                console.log("updatestatus ", requestQuery);
                var bankAddress = requestQuery.bankAddress ? requestQuery.bankAddress : '';
                var kycInformation = await updateKycStatus(requestQuery.address, requestQuery.status, bankAddress);
                res.status(200);
                res.send(kycInformation);
            } catch (err) {
                console.log("Console Error ", err);
                res.status(500);
                res.json(err);
            }
        });

        async function createBCUser(userName, password) {
            var usersAddress = '';
            console.log("userName ");
            await web3.eth.personal.newAccount(password).then(function(result) {
                usersAddress = result;
                console.log("usersAddress ", usersAddress);
                web3.eth.personal.unlockAccount(usersAddress, password, 0).then(console.log('Account unlocked!'));
            })
            return usersAddress;
        }

        async function deployUserContract() {
            //exports.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
            if (Object.keys(keyMapUserInfo).length == 0) {
                try {
                    var defaultAccount = '';
                    await web3.eth.getAccounts().then(function (accounts) {
                        defaultAccount = accounts[0];
                    });
                    console.log("compiledObject", compiledObject);
                    var contract = compiledObject.contracts[':UserInfo'];
                    var userAbi = JSON.parse(contract.interface);
                    var userBytecode = '0x' + contract.bytecode;
                    var gasEstimate = 1500000;
                    await web3.eth.estimateGas({ from: defaultAccount, data: userBytecode }).then(function (gasAmount) {
                        gasEstimate = gasAmount;
                    }).catch(function (error) {
                        console.log("gasAmount error", error);
                    });
                    var myContract = new web3.eth.Contract(userAbi);
                    var contractAddress = '';
                    await myContract.deploy({
                        data: userBytecode
                    }).send({
                        from: defaultAccount,
                        gas: gasEstimate
                    }, function (error) {
                    }).on('receipt', function (receipt) {
                        console.log("receipt", receipt.contractAddress); // contains the new contract address
                        contractAddress = receipt.contractAddress;
                    }).then(function (newContractInstance) {
                        contractAddress = newContractInstance.options.address;
                        console.log("newContractInstance", newContractInstance.options.address); // instance with the new contract address
                    });
                    console.log("contractInfo After AWAIT");
                    keyMapUserInfo = { contractId: contractAddress, contractABI: userAbi, defaultAccount: defaultAccount };
                }
                catch (error) {
                    console.log("error", error);
                }
            }
        }

        async function createUser(userInfo) {
            console.log("keyMapUserInfo ", keyMapUserInfo);
            var contractAbi = keyMapUserInfo.contractABI;
            var contractAddress = keyMapUserInfo.contractId;
            var fromAddress = keyMapUserInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            console.log("userInfo", ...userInfo);
            // console.log("contractAbi", contractAbi);
            // console.log("contractAddress", contractAddress);
            // console.log("fromAddress", fromAddress);
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                var params = [userInfo[0], userInfo[1],userInfo[2], userInfo[3], userInfo[4]];
                console.log("params params ", ...params);
                await Contract.methods.insertUser(...userInfo).send(transactionObject)
                    .on('transactionHash', function(hash){
                        console.log("transactionHash", hash)
                }).catch(function(err) {
                    console.log("Contract Write Error ", err);
                })

                await Contract.methods.getallUser().call().then(function(result) {
                    console.log("Contract Read result ", result);
                }).catch(function(err) {
                    console.log("Contract Read Error ", err);
                });
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function getUserDetails() {
            var userDetails = [];
            if (Object.keys(keyMapUserInfo).length == 0) {
               await deployUserContract();
            }
            var contractAbi = keyMapUserInfo.contractABI;
            var contractAddress = keyMapUserInfo.contractId;
            var fromAddress = keyMapUserInfo.defaultAccount;
            console.log("keyMapUserInfo ", keyMapUserInfo);
            var validContractId = web3.eth.getCode(contractAddress);

            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                var usersAddress = []; 
                await Contract.methods.getallUser().call().then(function(result) {
                    console.log("Contract Read result ", result);
                    usersAddress = result;
                }).catch(function(err) {
                    console.log("Contract Read Error ", err);
                });
                
                for(row=0; row < usersAddress.length; row++) {
                    await Contract.methods.getUser(usersAddress[row]).call().then(function(result) {
                        
                        var userDetail = { accountId: result.accountId,
                                        accountName: result.accountName,
                                        password: result.password,
                                        amount: result.amount,
                                        status: result.status,
                                        userType: result.userType,
                                        }
                        console.log("Contract getUser userDetail ", userDetail );
                        userDetails.push(userDetail);
                    }).catch(function(err) {
                        console.log("Contract getUser Error ", err);
                    });
                }
                console.log("userDetails", userDetails);
                return userDetails;
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function updateUserInfo(method, address, data) {

            var contractAbi = keyMapUserInfo.contractABI;
            var contractAddress = keyMapUserInfo.contractId;
            var fromAddress = keyMapUserInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            console.log("Method", method);
            console.log("address", address);
            console.log("data", data);
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                await Contract.methods[method](address, data).send(transactionObject)
                    .on('transactionHash', function(hash){
                        console.log("transactionHash", hash)
                        return true;
                }).catch(function(err) {
                    console.log("Contract Write Error ", err);
                })
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function deployUserKycContract() {
            //exports.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
            if (Object.keys(keyMapUserKycInfo).length == 0) {
                try {
                    var defaultAccount = '';
                    await web3.eth.getAccounts().then(function (accounts) {
                        defaultAccount = accounts[0];
                    });
                    //console.log("compiledObject", compiledObject);
                    var personalContract = compiledObject.contracts[':UserPersonalInfo'];
                    var userPersonalAbi = JSON.parse(personalContract.interface);
                    var userPersonalBytecode = '0x' + personalContract.bytecode;
                    var gasEstimate = 1500000;
                    await web3.eth.estimateGas({ from: defaultAccount, data: userPersonalBytecode }).then(function (gasAmount) {
                        gasEstimate = gasAmount;
                    })
                    var kycContract = new web3.eth.Contract(userPersonalAbi);
                    var kycContractAddress = '';
                    await kycContract.deploy({
                        data: userPersonalBytecode
                    }).send({
                        from: defaultAccount,
                        gas: gasEstimate
                    }, function (error) {
                    }).on('receipt', function (receipt) {
                        console.log("receipt", receipt.contractAddress); // contains the new contract address
                        kycContractAddress = receipt.contractAddress;
                    }).then(function (newContractInstance) {
                        kycContractAddress = newContractInstance.options.address;
                        console.log("newContractInstance", newContractInstance.options.address); // instance with the new contract address
                    });
                    console.log("contractInfo After AWAIT");
                    keyMapUserKycInfo = { kycContractId: kycContractAddress, kycContractABI: userPersonalAbi, defaultAccount: defaultAccount };
                }
                catch (error) {
                    console.log("error", error);
                }
            }
        }
        
        async function updateUserKycInfo(method, address, data) {

            var contractAbi = keyMapUserKycInfo.kycContractABI;
            var contractAddress = keyMapUserKycInfo.kycContractId;
            var fromAddress = keyMapUserKycInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            console.log("Method", method);
            console.log("address", address);
            
            
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                await Contract.methods[method](address, ...data).send(transactionObject)
                    .on('transactionHash', function(hash){
                        console.log("transactionHash", hash)
                        map.push(hash)
                        //keyUserTransaction.concat({[address]: map});
                        finalObject = keyUserTransaction.concat({[address]: map});
                        keyUserTransaction = finalObject; 
                        console.log("finalObject ", finalObject)
                        return true;
                }).catch(function(err) {
                    console.log("Contract Write Error ", err);
                })
                console.log("keyUserTransaction", keyUserTransaction);
                await Contract.methods.getUsersKycInfo(address).call().then(function(result) {
                    console.log("Contract getUsersKycInfo result ", result.kycInformation);
                }).catch(function(err) {
                    console.log("Contract Read Error ", err);
                });
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function getUserKycInfo(method, address) {

            var contractAbi = keyMapUserKycInfo.kycContractABI;
            var contractAddress = keyMapUserKycInfo.kycContractId;
            var fromAddress = keyMapUserKycInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            console.log("method", method);
            console.log("address", address);
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var userPersonalKycInfo = []; 
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                await Contract.methods[method](address).call().then(function(result) {
                    console.log("Contract getUsersKycInfo result ", result);
                    if(method == 'getUsersPersonalInfo') {
                        userPersonalKycInfo = { name: result.name,
                                                gender: result.dob,
                                                userHomeAddress: result.userHomeAddress,
                                                phone: result.phone
                                              }
                    } else {

                        var user = result.kycInformation;
                        for (key in user) {
                            if (user.hasOwnProperty("idproof")) {
                                console.log(key + " = " + user[key]);
                            }
                        } 
                        userPersonalKycInfo = { status: result.status,
                                                kycInformation: result.kycInformation,
                                                dataAccessInfo: result.dataAccessInfo
                                              }
                    }
                });
                return userPersonalKycInfo;
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function uploadFilesToIpfs(files, filePath) {
            
            await ipfs.files.add(files, async function (err, files) {
                if(err) {
                    console.log("uploadFilesToIpfs", err);
                } else {
                    await sleep(1000)
                    const ipfsHash = await files[0].hash;
                    console.log(ipfsHash);
                    await fs.unlink(filePath, (err) => {
                        if (err) 
                            console.log('Unable to delete file');
                        else
                            return ipfsHash;
                            console.log('File deleted from filesystem');
                      });
                }
                ;
            })
        }

        async function getAllKycInfo(identifier) {

            var contractAbi = keyMapUserKycInfo.kycContractABI;
            var contractAddress = keyMapUserKycInfo.kycContractId;
            var fromAddress = keyMapUserKycInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var userPersonalKycInfo = []; 
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                var usersAddress = []; 
                await Contract.methods.getallUserPersonalInfo().call().then(function(result) {
                    console.log("Contract Read result ", result);
                    usersAddress = result;
                }).catch(function(err) {
                    console.log("Contract Read Error ", err);
                });
                
                for(row=0; row < usersAddress.length; row++) {
                    await Contract.methods.getUsersKycInfo(usersAddress[row]).call().then(function(result) {
                        
                        if(identifier == result.identifier) {
                            console.log("Contract getUser userPersonalKycDetail ", result );
                            var userPersonalKycDetail = { name: result.name,
                                                          kycInfo: result.kycInformation,
                                                          status: result.status,
                                                          identifier: result.identifier,
                                                          accountId: usersAddress[row]
                                                        }
                            userPersonalKycInfo.push(userPersonalKycDetail);
                        }
                        
                    }).catch(function(err) {
                        console.log("Contract getUser Error ", err);
                    });
                }
                console.log("userPersonalKycInfo userPersonalKycInfo", userPersonalKycInfo);
                return userPersonalKycInfo;
            } catch (error) {
                console.log("Error ", error);
            }
        }

        async function createBankContainer(bankName) {
            
            var cName=bankName;
            var p1,p2,getPort;
            (async () => {
                p1=await getPort();
                p2=await getPort();
                getPort=await getPort();
            })();

            docker.createContainer({Image: 'kyc-image:latest', name:cName, "HostConfig": {
                "PortBindings": { "80/tcp": [{ "HostPort": p1 }],"5000/tcp": [{ "HostPort": p2 }],"8545/tcp": [{ "HostPort":getPort }] }
            }, 
                "ExposedPorts": { "5000/tcp": {},"80/tcp":{} }
            }, async function (err, container)  {
                    await container.start(function (err, data)  {
                    if(err)
                        console.log('Unable to create container: '+ cName);
                    else  
                        console.log('successfully created container: '+ cName);
                    });
                var gethURL = 'http://localhost:'+getPort;
                return gethURL;
            });
        }

        async function getBankUserDetails() {
            var userDetails = [];
            if (Object.keys(keyMapUserInfo).length == 0) {
               await deployUserContract();
            }
            var contractAbi = keyMapUserInfo.contractABI;
            var contractAddress = keyMapUserInfo.contractId;
            var fromAddress = keyMapUserInfo.defaultAccount;
            console.log("keyMapUserInfo ", keyMapUserInfo);
            var validContractId = web3.eth.getCode(contractAddress);

            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                var usersAddress = []; 
                await Contract.methods.getallUser().call().then(function(result) {
                    console.log("Contract Read result ", result);
                    usersAddress = result;
                }).catch(function(err) {
                    console.log("Contract Read Error ", err);
                });
                
                for(row=0; row < usersAddress.length; row++) {
                    await Contract.methods.getUser(usersAddress[row]).call().then(function(result) {
                        
                        if(result.userType == 'type') {
                            var userDetail = { accountId: result.accountId,
                                            accountName: result.accountName,
                                            password: result.password,
                                            amount: result.amount,
                                            status: result.status,
                                            userType: result.userType,
                                            }
                        }
                        console.log("Contract getUser userDetail ", userDetail );
                        userDetails.push(userDetail);
                    }).catch(function(err) {
                        console.log("Contract getUser Error ", err);
                    });
                }
                console.log("userDetails", userDetails);
                return userDetails;
            } catch (error) {
                console.log("Error ", error);
            }
        }

        function sleep(ms){
            return new Promise(resolve=>{
                setTimeout(resolve,ms)
            })
        }

        async function getTransactionsByAccount(myaccount, startBlockNumber, endBlockNumber) {
            if (endBlockNumber == null) {
              endBlockNumber = await web3.eth.blockNumber;
              console.log("Using endBlockNumber: " + endBlockNumber);
            }
            if (startBlockNumber == null) {
              startBlockNumber = endBlockNumber - 1000;
              console.log("Using startBlockNumber: " + startBlockNumber);
            }
            console.log("Searching for transactions to/from account \"" + myaccount + "\" within blocks "  + startBlockNumber + " and " + endBlockNumber);
            var transactions = [];
            for (var i = startBlockNumber; i <= endBlockNumber; i++) {
              if (i % 1000 == 0) {
                console.log("Searching block " + i);
              }
              var block = await web3.eth.getBlock(i, true);
              if (block != null && block.transactions != null) {
                block.transactions.forEach( function(e) {
                  if (myaccount == "*" || myaccount == e.from || myaccount == e.to) {
                    transactions.push(e);
                    console.log("  tx hash          : " + e.hash + "\n"
                      + "   nonce           : " + e.nonce + "\n"
                      + "   blockHash       : " + e.blockHash + "\n"
                      + "   blockNumber     : " + e.blockNumber + "\n"
                      + "   transactionIndex: " + e.transactionIndex + "\n"
                      + "   from            : " + e.from + "\n" 
                      + "   to              : " + e.to + "\n"
                      + "   value           : " + e.value + "\n"
                      + "   time            : " + block.timestamp + " " + new Date(block.timestamp * 1000).toGMTString() + "\n"
                      + "   gasPrice        : " + e.gasPrice + "\n"
                      + "   gas             : " + e.gas + "\n"
                      + "   input           : " + e.input);
                  }
                })
              }
            }
            return transactions;
    }
    
    async function getUserTransaction(address) {
        
        var transactions;
        var test = address;
        var transactionInfo = [];
        if(keyUserTransaction != '') {
            for(var i=0; i< keyUserTransaction.length; i++) {
                var key = Object.keys(keyUserTransaction[i]);
                if(key[0] == address) {
                    transactions = keyUserTransaction[i];
                    transactions = transactions[address];
                }
            }
            for(var j=0; j< transactions.length; j++) {
                transaction = await web3.eth.getTransaction(transactions[j]);
                blockInfo = await web3.eth.getBlock(transaction.blockNumber);
                blockInfo.input = transaction.input; 
                transactionInfo.push(blockInfo);
            }
        }
        return transactionInfo;
    }

    async function getBankTransaction(address) {
        
        var transactions;
        var test = address;
        var transactionInfo = [];
        if(keyBankTransaction != '') {
            for(var i=0; i< keyBankTransaction.length; i++) {
                var key = Object.keys(keyBankTransaction[i]);
                if(key[0] == address) {
                    transactions = keyBankTransaction[i];
                    transactions = transactions[address];
                }
            }
            for(var j=0; j< transactions.length; j++) {
                transaction = await web3.eth.getTransaction(transactions[j]);
                blockInfo = await web3.eth.getBlock(transaction.blockNumber);
                blockInfo.input = transaction.input; 
                transactionInfo.push(blockInfo);
            }
        }
        return transactionInfo;
    }

    async function updateKycStatus(address, status, bankAddress) {
            
            var contractAbi = keyMapUserKycInfo.kycContractABI;
            var contractAddress = keyMapUserKycInfo.kycContractId;
            var fromAddress = keyMapUserKycInfo.defaultAccount;
            var validContractId = web3.eth.getCode(contractAddress);
            
            if(validContractId.length <= 3){
                res.status(400);
                res.json("Invalid contract id");
            }
            try {
                var transactionObject = {
                    from: fromAddress,
                    gas: 1500000,
                    gasPrice: '30000000000000'
                };
                var Contract = new web3.eth.Contract(contractAbi, contractAddress, transactionObject);
                await Contract.methods.updateStaus(address, status).send(transactionObject)
                    .on('transactionHash', function(hash){
                        console.log("transactionHash updateKycStatus", hash)
                        map.push(hash)
                        finalObject = keyUserTransaction.concat({[address]: map});
                        keyUserTransaction = finalObject;
                        if( bankAddress != '') {
                            bankMap.push(hash)
                            bankObject = keyBankTransaction.concat({[bankAddress]: map});
                            keyBankTransaction = bankObject;
                        }
                        /* var keyBankTransaction=[];
                        var bankMap = []; */
                        return true;
                }).catch(function(err) {
                    console.log("Contract Write Error ", err);
                })
            } catch (error) {
                console.log("Error ", error);
            }
    }

    };
    return IndexRoute;
})();
exports.IndexRoute = IndexRoute;
