import { NextFunction, Request, Response, Router } from "express";
import * as solc from "solc";
import * as fs from "fs";
import * as request from "request";
import * as dir from "node-dir";
import * as archiver from "archiver";
import * as Web3 from "web3";

const out = fs.openSync('./out.log', 'a');
const err = fs.openSync('./out.log', 'a');
const {Pool,Client } = require('pg');
const { spawn,exec } = require('child_process');
let connectionString = process.env.DATABASE_URL;

const pool = new Pool({
    connectionString: connectionString,
  });

let blockchainurl = process.env.BLOCKCHAIN_SERVICE_URL || 'http://localhost:8545';
let web3 = new Web3(new Web3.providers.HttpProvider(blockchainurl));
var solFile = fs.readFileSync('./assets/userInfo.sol', "utf8");
var compiledObject = solc.compile(solFile, 1);
var keyMapUserInfo={};
/**
 * / route
 *
 * @class User
 */
export class IndexRoute {
  /**
   * Create the routes.
   *
   * @class Ind-exRoute
   * @method create
   * @static
   */
  public static create(router: Router) {

	router.get("/bcurl", function (req, res, next) {
        let blockchainurl = process.env.BLOCKCHAIN_SERVICE_URL;
        let privateIp = process.env.HEROKU_PRIVATE_IP;
        var postgresurl = process.env.BLOCKCHAIN_BACKUP_DIRECTORY;
        console.log(process.env.BLOCKCHAIN_SERVICE_URL);
        console.log(blockchainurl);
        console.log(process.env.HEROKU_PRIVATE_IP);
        console.log(process.env.HEROKU_DNS_APP_NAME);
        console.log(process.env.HEROKU_DNS_DYNO_NAME);
        console.log(process.env.HEROKU_DNS_FORMATION_NAME);
        res.send({"blockchainurl":blockchainurl, "privateIp":privateIp, "postgresurl": postgresurl});
    });

    router.get("/sol", (req: Request, res: Response, next: NextFunction) => {
        var input = fs.readFileSync('./assets/sample.sol', "utf8")
        //var output = solc.compile(input, 1)
        res.send(input);
    });

    router.post("/solstring", (req: Request, res: Response, next: NextFunction) => {
      // console.log(req);
      // res.send(req.body.request);
      //var output = solc.compile(req, 1)
      //res.send(output);
	  try {
		var output = solc.compile(req.body.source, 1)
		res.send(output);
	  } catch(e) {
		//res.send(e);
	  }
    });

    router.get("/create/account", function (req, res, next) {
        
        try {
            var userContractInfo = JSON.parse(keyMapUserInfo);
            console.log("userContractInfo", userContractInfo)
            if(keyMapUserInfo) {
                console.log("keyMapUserInfo", keyMapUserInfo)
                res.status(200);
                res.json(keyMapUserInfo);
            } else {
                deployUserContract;
            }

        }  catch (err) {
            console.log("Console Error ", err);
            res.status(500);
            res.json(err);
        }
        
        /*request.post({
                "headers": { "content-type": "application/json" },
                "url": "https://g5zr7zqn17.execute-api.us-east-1.amazonaws.com/dev/datastore",
                "body": JSON.stringify(req.body)
            }, (error, response, body) => {
                if(error) {
                    res.send(error);
                }
                    res.send(body);
            });*/
    });

    router.get("/accounts", function (req, res, next) {
        request.get({
                "headers": { "content-type": "application/json" },
                "url": "https://g5zr7zqn17.execute-api.us-east-1.amazonaws.com/dev/datastore/"+req.query.host,
            }, (error, response, body) => {
                if(error) {
                    res.send(error);
                }
                    res.send(body);
            });
    });
	router.get("/loanappurl", function (req, res, next) {
		var loanappurl = process.env.BLOCKCHAIN_LOAN_APP_URL;
		res.send({ "url": loanappurl });
    });
    
     router.get("/backupurl", function (req, res, next) {
        var backupurl = process.env.BLOCKCHAIN_BACKUP_URL;
		res.send({ "url": backupurl });
    }); 

    router.get("/dburl", function (req, res, next) {
        var dburl = process.env.DATABASE_URL;
        res.send({ "url": dburl});
    });
    
    router.get("/backupdata", function (req, res, next) {

        console.log(process.env.BLOCKCHAIN_BACKUP_DIRECTORY);
        var backupDirectory = process.env.BLOCKCHAIN_BACKUP_DIRECTORY || "/home/eth_runner/myeth/node";
        try {
            const gethStop = spawn('sh',['/home/eth_runner/myeth/killgeth.sh']);
            gethStop.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
            });
            
            gethStop.stderr.on('error', (data) => {
            console.log(`stderr: ${data}`);
            });
            
            gethStop.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            gethBackup(backupDirectory);
            });
            res.status(200);
            res.send({ "response": "success" });
        } catch (err) {
            console.log("Console Error ", err);
            res.status(500);
            res.json(err);
        }
    
    });

    function gethBackup(bckDir)
    {
        
         pool.query('CREATE TABLE IF NOT EXISTS HEROKUADD (filename text,'+
        'file bytea ,flag character(1),sno bigint,created_date timestamp,restore_date timestamp)',createTable);

    }
      
   async function list(err,files)
    {
        if (err) throw err;
        try {
            
            for(let fl of files)
            {

                await fs.readFile(fl,function (err, data){
                    //client.connect();
                    pool.query('INSERT INTO HEROKUADD (FILENAME,FILE,FLAG,CREATED_DATE,RESTORE_DATE) VALUES ($1,$2,$3,$4,$5)',
                                [fl,data,'A',new Date(),null],
                    function(err, writeResult) 
                    {
                        if(err == null) {
                            console.log('Zip file inserted');
                            fs.unlinkSync(fl);
                            console.log('zip file removed:'+fl);
                            const { exec } = require('child_process');
                            exec('echo "test" > /home/eth_runner/myeth/test.txt');  
                            console.log('Backup created');                      
                            
                        }
                        else
                            console.log('insert err',err);
                    // client.release();
                    });  
                    
                    
                });
            }
            } catch(error) {
                console.log('list catch error', error);
            }
    }
    function createTable(err, result)
    {
        if(err!=null)
          console.log('Error in creating table');
        else
         { 
             console.log('Table created/Table alredy exist');
             archive();
         }
    }

    function archive()
    {
        var backupDirectory = process.env.BLOCKCHAIN_BACKUP_DIRECTORY || "/home/eth_runner/myeth/node";
        var output = fs.createWriteStream('/home/eth_runner/myeth/backup/'+new Date()+'.zip');
        var archive = archiver('zip');

        output.on('close', function() 
        {
             console.log(archive.pointer() + ' total bytes');
             console.log('archiver has been finalized and the output file descriptor has closed.');
             dir.files('/home/eth_runner/myeth/backup',list);  
        });

        archive.on('error', function(err) 
        {
            throw err;
        });

        archive.pipe(output);

        archive.directory(backupDirectory,false).finalize();
    }

    async function deployUserContract() {
            
        //exports.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
        try {
            var defaultAccount = '';
            await web3.eth.getAccounts().then(function (accounts) {
                defaultAccount = accounts[0];
            });
            var contract = compiledObject.contracts[':UserInfo'];
            var userAbi = JSON.parse(contract.interface);
            var userBytecode = '0x' + contract.bytecode;
            var gasEstimate = 1500000;
            await web3.eth.estimateGas({ from: defaultAccount, data: userBytecode }).then(function(gasAmount){
                gasEstimate = gasAmount;
            }).catch(function(error){
                console.log("gasAmount error", error);
            })
            var myContract = new web3.eth.Contract(userAbi);
            var contractAddress = '';
            await myContract.deploy({
                data: userBytecode,
            }).send({
                from: defaultAccount,
                gas: gasEstimate
            }, function(error){
            }).on('receipt', function(receipt){
               console.log("receipt", receipt.contractAddress) // contains the new contract address
               contractAddress = receipt.contractAddress;
            }).then(function(newContractInstance){
                contractAddress = newContractInstance.options.address;
                console.log("newContractInstance", newContractInstance.options.address) // instance with the new contract address
            });
            console.log("contractInfo After AWAIT");
            keyMapUserInfo = {contractId : contractAddress, contractABI: userAbi}
        }
        catch (error) {
            console.log("error", error);
        }
    }

    async function getUserInfo() {
        try {
            var defaultAccount = '';
            await web3.eth.getAccounts().then(function (accounts) {
                defaultAccount = accounts[0];
            });
            var contract = compiledObject.contracts[':UserInfo'];
            var userAbi = JSON.parse(contract.interface);
            var userBytecode = '0x' + contract.bytecode;
            var gasEstimate = 1500000;
            await web3.eth.estimateGas({ from: defaultAccount, data: userBytecode }).then(function(gasAmount){
                gasEstimate = gasAmount;
            }).catch(function(error){
                console.log("gasAmount error", error);
            })
            var myContract = new web3.eth.Contract(userAbi);
            var contractAddress = '';
            await myContract.deploy({
                data: userBytecode,
            }).send({
                from: defaultAccount,
                gas: gasEstimate
            }, function(error){
            }).on('receipt', function(receipt){
               console.log("receipt", receipt.contractAddress) // contains the new contract address
               contractAddress = receipt.contractAddress;
            }).then(function(newContractInstance){
                contractAddress = newContractInstance.options.address;
                console.log("newContractInstance", newContractInstance.options.address) // instance with the new contract address
            });
            console.log("contractInfo After AWAIT");
            keyMapUserInfo = {contractId : contractAddress, contractABI: userAbi}
        }
        catch (error) {
            console.log("error", error);
        }
    }
  }
}
